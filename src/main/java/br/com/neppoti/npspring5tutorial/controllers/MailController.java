package br.com.neppoti.npspring5tutorial.controllers;

import javax.mail.MessagingException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.neppoti.npspring5tutorial.mail.MailSender;

@RestController
public class MailController {
	
	private MailSender mailSender;

	public MailController(MailSender smtp) {
		this.mailSender = smtp;
	}

	@RequestMapping("/mail")
	public String mail() throws MessagingException {
		
		mailSender.send("jefferson.ufu@gmail.com", "E-mail serio.", "<h1>Este e-mail de teste e' serio.</h1>");
		
		return "E-mail enviado!";
	}
}